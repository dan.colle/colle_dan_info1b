# lister la table t_personnes
SELECT * FROM t_personnes;


# lister la table t_mails
SELECT * FROM t_mails;


# Lister la table t_pays
SELECT * FROM t_pays;

# Lister les pays, et leur id dans l'ordre alphabétiue
SELECT nom_fr_fr AS 'Pays', id_pays AS 'id' FROM t_pays
ORDER BY nom_fr_fr;

# Lister la table t_pers_avoir_mail
SELECT * FROM t_pers_avoir_mail;


# Lister l'id, le prenom, le nom, l'addresse et la date de naissance d'une personne
SELECT id_personne, prenom_personne, nom_personne, adresse, date_naissance
FROM t_personnes;


# Idem, mais en utilisant des alias
SELECT id_personne AS 'id', prenom_personne AS 'Prenom', nom_personne AS 'Nom', adresse AS 'Adresse', date_naissance AS 'Date de naissance'
FROM t_personnes;


# Lister l'id, le prenom, le nom, l'addresse et la date de naissance d'une personne. Ainsi que son id de sexe et pays (FK). En utilisant des alias
SELECT id_personne AS 'id', prenom_personne AS 'Prenom', nom_personne AS 'Nom', adresse AS 'Adresse', date_naissance AS 'Date de naissance', fk_sexe, fk_pays
FROM t_personnes;


# Lister l'id, le prenom, le nom, l'addresse et la date de naissance d'une personne. Ainsi que son id de sexe et pays (FK). En utilisant des alias
# Lister uniquement les hommes
SELECT id_personne AS 'id', prenom_personne AS 'Prenom', nom_personne AS 'Nom', adresse AS 'Adresse', date_naissance AS 'Date de naissance', fk_sexe, fk_pays
FROM t_personnes
WHERE fk_sexe = '1';

# Lister l'id, le prenom, le nom, l'addresse et la date de naissance d'une personne. Ainsi que son id de sexe et pays (FK). En utilisant des alias
# Lister uniquement les Albanais
SELECT * FROM t_personnes
WHERE fk_pays = 2;

# Lister l'id, le prenom, le nom, l'addresse et la date de naissance d'une personne. Ainsi que son id de sexe et pays (FK). En utilisant des alias
# Lister uniquement les femmes
SELECT id_personne AS 'id', prenom_personne AS 'Prenom', nom_personne AS 'Nom', adresse AS 'Adresse', date_naissance AS 'Date de naissance', fk_sexe, fk_pays
FROM t_personnes
WHERE fk_sexe = '2';



# Lister l'id, le prenom, le nom, l'addresse et la date de naissance d'une personne. Ainsi que son id de sexe et pays (FK). En utilisant des alias
# Lister unuiquement les personnes dont le prenom commence par "m"
SELECT id_personne AS 'id', prenom_personne AS 'Prenom', nom_personne AS 'Nom', adresse AS 'Adresse', date_naissance AS 'Date de naissance', fk_sexe, fk_pays
FROM t_personnes
WHERE prenom_personne LIKE 'm%';



# Lister l'id, le prenom, le nom, l'addresse et la date de naissance d'une personne. Ainsi que son id de sexe et pays (FK). En utilisant des alias
# Lister unuiquement les personnes dont le prenom fini par "n"
SELECT id_personne AS 'id', prenom_personne AS 'Prenom', nom_personne AS 'Nom', adresse AS 'Adresse', date_naissance AS 'Date de naissance', fk_sexe, fk_pays
FROM t_personnes
WHERE prenom_personne LIKE '%n';



# Lister l'id, le prenom, le nom, l'addresse et la date de naissance d'une personne. Ainsi que son id de sexe et pays (FK). En utilisant des alias
# Lister unuiquement les personnes dont le prenom comporte 5 caractères
SELECT id_personne AS 'id', prenom_personne AS 'Prenom', nom_personne AS 'Nom', adresse AS 'Adresse', date_naissance AS 'Date de naissance', fk_sexe, fk_pays
FROM t_personnes
WHERE prenom_personne LIKE '_____';



# Lister l'id, le prenom, le nom, l'addresse et la date de naissance d'une personne. Ainsi que son id de sexe et pays (FK). En utilisant des alias
# Lister unuiquement les personnes dont le prenom comporte 5 caractères
# Dans l'ordre alphabetique de leur nom
SELECT id_personne AS 'id', prenom_personne AS 'Prenom', nom_personne AS 'Nom', adresse AS 'Adresse', date_naissance AS 'Date de naissance', fk_sexe, fk_pays
FROM t_personnes
WHERE prenom_personne LIKE '_____'
ORDER BY nom_personne;



# Lister l'id, le prenom, le nom, l'addresse et la date de naissance d'une personne. Ainsi que son id de sexe et pays (FK). En utilisant des alias
# Lister unuiquement les personnes qui ont plus de 18ans
SELECT id_personne AS 'id', prenom_personne AS 'Prenom', nom_personne AS 'Nom', adresse AS 'Adresse', date_naissance AS 'Date de naissance', fk_sexe, fk_pays
FROM t_personnes
WHERE date_naissance < '2004-01-23';



# Lister l'id, le prenom, le nom, l'addresse et la date de naissance d'une personne. Ainsi que son id de sexe et pays (FK). En utilisant des alias
# Lister unuiquement les personnes qui ont moins de 18ans
SELECT id_personne AS 'id', prenom_personne AS 'Prenom', nom_personne AS 'Nom', adresse AS 'Adresse', date_naissance AS 'Date de naissance', fk_sexe, fk_pays
FROM t_personnes
WHERE date_naissance > '2004-01-23';



# Lister l'id, le prenom, le nom, l'addresse et la date de naissance d'une personne. Ainsi que son id de sexe et pays (FK). En utilisant des alias
# Lister aussi son id d'email (FK)
SELECT t_personnes.id_personne AS 'id', t_personnes.prenom_personne AS 'Prenom', t_personnes.nom_personne AS 'Nom', t_personnes.adresse AS 'Adresse', t_personnes.fk_sexe, t_personnes.fk_pays, t_personnes.date_naissance AS 'Date de naissance', t_pers_avoir_mail.fk_mail
FROM t_personnes
INNER JOIN t_pers_avoir_mail ON t_personnes.id_personne = t_pers_avoir_mail.fk_mail;




# Lister l'id, le prenom, le nom, l'addresse et la date de naissance d'une personne. Ainsi que son id de sexe et pays (FK). En utilisant des alias
# Lister aussi son email
SELECT id_personne AS 'id' , prenom_personne AS 'Prénom', nom_personne AS 'Nom' , adresse AS 'Adresse', t_mails.mail_personne AS 'E-mail',fk_sexe AS 'FK_sexe', fk_pays AS 'FK_pays', date_naissance AS 'Date de naissance'
FROM t_personnes
INNER JOIN t_pers_avoir_mail ON t_personnes.id_personne = t_pers_avoir_mail.fk_mail
INNER JOIN t_mails ON t_pers_avoir_mail.id_pers_avoir_mail = t_mails.id_mail;




# Lister l'id, le prenom, le nom, l'addresse et la date de naissance d'une personne, ainsi que son sexe et son e-mail. En utilisant des alias
SELECT id_personne AS 'id', prenom_personne AS 'Prenom', nom_personne AS 'Nom', adresse AS 'Adresse', date_naissance AS 'Date de naissance', t_mails.mail_personne AS 'E-mail', t_sexe.sexe_personne AS 'Sexe'
FROM t_personnes
INNER JOIN t_pers_avoir_mail ON t_personnes.id_personne = t_pers_avoir_mail.fk_mail
INNER JOIN t_mails ON t_pers_avoir_mail.id_pers_avoir_mail = t_mails.id_mail
INNER JOIN t_sexe ON t_personnes.fk_sexe = t_sexe.id_sexe;




# Lister l'id, le prenom, le nom, l'addresse et la date de naissance d'une personne, ainsi que son sexe, son e-mail et sa nationalité. En utilisant des alias
# Dans l'ordre Alphabétique du nom
SELECT id_personne AS 'id', prenom_personne AS 'Prenom', nom_personne AS 'Nom', adresse AS 'Adresse', t_pays.nom_fr_fr AS 'Nationalité', date_naissance AS 'Date de naissance', t_mails.mail_personne AS 'E-mail', t_sexe.sexe_personne AS 'Sexe'
FROM t_personnes
INNER JOIN t_pers_avoir_mail ON t_personnes.id_personne = t_pers_avoir_mail.fk_mail
INNER JOIN t_mails ON t_pers_avoir_mail.id_pers_avoir_mail = t_mails.id_mail
INNER JOIN t_sexe ON t_personnes.fk_sexe = t_sexe.id_sexe
INNER JOIN t_pays ON t_personnes.fk_pays = t_pays.id_pays
ORDER BY nom_personne;